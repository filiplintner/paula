<?php

class View{
	
	public function load($path){
		
		$path = (dirname(dirname(__FILE__)).'/../app/Views/'.$path.'.php');
		
		require $path;
		
	}
}
