<?php

class Handler{
	
	public function get_config(){
	
		$this->debug = Config_app::$debug;
		
		$this->custom_error = Config_app::$custom_error_page;
	}
		
	
	public function display(){
		
		echo "<pre>";
		echo $this->error['message']."<br>";
		echo $this->error['file'].", at line ".$this->error['line'];
		echo "<br><br><span style='padding-right:7px;'>Memory usage: ".(memory_get_peak_usage(true) /1024 / 1024)." MB</span>";
echo "|<span style='padding-left:7px;'>Generated in: ".(round((microtime(true) - $GLOBALS['start']) * 1000, 2))."ms<span>";
	}
	
	public function handle(){
		
		$this->error = error_get_last();
		
			if($this->debug){
				
				if($this->error != NULL){
					$this->display();
				}
				
				
			}else{
			
				if($this->error != NULL){
					
						$path = Config_app::$custom_error_page;
				
				include (dirname(dirname(__FILE__)).'/../app/Views/'.$path.'.php');
					
				}
			
				
			}
	
		

	}
	
	public function load(){
		
		$this->get_config();
		$this->handle();
		
	}
	
}




function shutdown(){
	
	$Handler = new Handler;
	$Handler->load();
	
}

error_reporting(0);
register_shutdown_function("shutdown");


