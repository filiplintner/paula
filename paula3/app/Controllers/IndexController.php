<?php

class IndexController extends BaseController{
	
	public function index(){
		#var_dump($args);
		$this->model->load('index_model');
		$this->model->index_model->all();
		
		$this->view->mydbresult = $this->model->index_model->result;
		$this->view->url = $this->helpers->url();
		$this->view->load('index_view');
	}
	
}

